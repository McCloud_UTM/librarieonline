package com.pronline.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pronline.*
import com.pronline.data.network.NetPath
import com.pronline.ui.home.HomeActivity
import com.pronline.util.UserInfo
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_login_btn.setOnClickListener {
            val usern = login_username_editText.text.toString()
            val passw = login_password_editText.text.toString()
            val url = caleApi(usern, passw)
            Log.d("Debug:","url="+url)
            val queue: RequestQueue = Volley.newRequestQueue(this)
            // Request a string response from the provided URL.
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    if (response==("1")) {
                        UserInfo.username = usern
                        val intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                    }else
                        Toast.makeText(this, "Username does not exist", Toast.LENGTH_LONG).show()

                },
                Response.ErrorListener { error ->
                    Toast.makeText(this, "Eroare conexiune", Toast.LENGTH_LONG).show()
                })

            // Add the request to the RequestQueue.
            queue.add(stringRequest)
    }

        back_to_register_text_view.setOnClickListener {
            // Log.d("LoginActivity","Try to show register activity")
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

    }
   private fun caleApi(usern:String, passw:String):String {
       val url = NetPath.pathApi + "login.php/?username=" + usern + "&password=" + passw
       return url
   }
    private fun makeRequest(url:String):Int{
        val queue: RequestQueue = Volley.newRequestQueue(this)
        var rasp = 0
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                if (response==("1"))
                    rasp = 1
            },
            Response.ErrorListener { error ->
                    rasp = 2
            })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
        return rasp
    }

}
