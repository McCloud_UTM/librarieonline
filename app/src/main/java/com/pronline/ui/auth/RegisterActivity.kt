package com.pronline.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pronline.ui.home.HomeActivity
import com.pronline.R
import com.pronline.data.network.NetPath
import com.pronline.util.UserInfo
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        register_register_btn.setOnClickListener {
            val usern = register_username_editText.text.toString()
            val passw = register_password_editText.text.toString()
            val cpassw = register_confirm_password_editText.text.toString()
            val fullname = register_full_name_editText.text.toString()
            val email = register_email_editText.text.toString()

            val confirm = verify_passw(passw, cpassw)
            if(confirm==1){
                val url = getCale(usern, passw, fullname, email)

                val queue:RequestQueue = Volley.newRequestQueue(this)

                // Request a string response from the provided URL.
                val stringRequest = StringRequest(Request.Method.GET, url,
                    Response.Listener<String> { response ->
                        // Display the first 500 characters of the response string.
                        if (response==("0"))
                            Toast.makeText(this, "Username already exist", Toast.LENGTH_LONG).show()
                        else {
                            // user created succesfully
                            UserInfo.username =register_username_editText.toString()
                            val intent = Intent(this, HomeActivity::class.java)
                            startActivity(intent)
                        }

                    },
                    Response.ErrorListener {error ->
                       // println(error.toString())
                       Toast.makeText(this, "Eroare conexiune "+error.toString(), Toast.LENGTH_LONG).show()
                    })

                // Add the request to the RequestQueue.
                queue.add(stringRequest)
                finish()
            } else {
                Toast.makeText(this, "Password not match", Toast.LENGTH_LONG).show()
            }

        }

        back_to_login_text_view.setOnClickListener {
            finish()
        }
    }
    private fun verify_passw(passw:String, cpassw:String):Int{
        if(passw.equals(cpassw))
            return 1
        else
            return 0
    }
    private fun getCale(usern:String, passw:String, fullname:String, email: String): String{
        val url = NetPath.pathApi +"add_user.php" +
                "?username="+ usern +
                "&password="+ passw +
                "&name="+ fullname +
                "&email="+ email
        return url
    }
}