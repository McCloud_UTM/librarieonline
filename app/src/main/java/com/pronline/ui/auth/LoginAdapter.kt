package com.pronline.ui.auth

import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pronline.data.network.NetPath

class LoginAdapter : AppCompatActivity(){
    private fun caleApi(usern:String, passw:String):String {
        val url = NetPath.pathApi + "login.php/?username=" + usern + "&password=" + passw
        return url
    }
    private fun makeRequest(url:String):Int{
        val queue: RequestQueue = Volley.newRequestQueue(this)
        var rasp = 0
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(
            Request.Method.GET, url,
            Response.Listener<String> { response ->
                if (response==("1"))
                    rasp = 1
            },
            Response.ErrorListener { error ->
                rasp = 2
            })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
        return rasp
    }
}