package com.pronline.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pronline.R
import com.pronline.data.network.NetPath
import com.pronline.ui.order.OrderActivity
import com.pronline.util.UserInfo

class QtyFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val v = inflater!!.inflate(R.layout.fragment_qty, container, false)
        val numberPieces=v.findViewById<EditText>(R.id.enter_quantity_textView)
        val btnUpdateQty = v.findViewById<Button>(R.id.quantity_update_btn)

        btnUpdateQty.setOnClickListener {
            val url = NetPath.pathApi+ "add_order.php?username="+UserInfo.username+
                    "&idbook="+UserInfo.itemId +
                    "&quantity="+numberPieces.text.toString()
            val requestQ:RequestQueue=Volley.newRequestQueue(activity)
            val stringReq=StringRequest(Request.Method.GET,url,Response.Listener { response->
                val intentAct=Intent(activity, OrderActivity::class.java)
                startActivity(intentAct)

            }, Response.ErrorListener { error->
                Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show()
            })
            requestQ.add(stringReq)
        }

        return v
    }

}
