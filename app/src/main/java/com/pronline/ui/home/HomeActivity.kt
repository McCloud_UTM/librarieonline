package com.pronline.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.pronline.ui.books.BooksActivity
import com.pronline.R
import com.pronline.data.network.NetPath
import com.pronline.util.UserInfo
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val url = NetPath.pathApi +"get_category.php"
        val list=ArrayList<String>()
        val request:RequestQueue=Volley.newRequestQueue(this)
        val jsonArray = JsonArrayRequest(Request.Method.GET,url,null, Response.Listener { response ->

            for(x in 0 until response.length()-1)
                list.add(response.getJSONObject(x).getString("category"))
            val adapterList = ArrayAdapter(this,
                R.layout.book_list_view,list)
            home_list_category.adapter = adapterList
        }, Response.ErrorListener { error ->
            Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
        })
        request.add(jsonArray)

        home_list_category.setOnItemClickListener { parent, view, position, id ->
            val category:String=list[position]
            val obj = Intent(this, BooksActivity::class.java)
            obj.putExtra("category", category)
            startActivity(obj)
        }
    }
}
