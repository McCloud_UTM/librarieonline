package com.pronline.ui.books


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.pronline.ui.fragments.QtyFragment
import com.pronline.R
import com.pronline.data.network.NetPath
import com.pronline.data.repositories.Book
import com.pronline.util.UserInfo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.book_row.view.*

class BookAdapter(var context:Context, var  list:ArrayList<Book>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemHolder).bind(list[position].title, list[position].author, list[position].price, list[position].photo, list[position].id)
            }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v:View=LayoutInflater.from(context).inflate(R.layout.book_row, parent, false)
        return ItemHolder(v)
    }

    class ItemHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun bind(t:String, a:String, p:Double, u:String, item_id:Int){
            itemView.book_title.text=t
            val autor = "Autor: "+a
            itemView.book_author.text=autor
            val pret = "Pret: "+p.toString()+" RON"
            itemView.book_price.text= pret
            var web:String= NetPath.pathApi+"images/"+u
            web=web.replace(" ","%20")
            Picasso.get().load(web).into(itemView.book_imageView)
          itemView.add_to_chart_btn.setOnClickListener {
                UserInfo.itemId=item_id
                val myDialog = QtyFragment()
                val manager = (itemView.context as FragmentActivity).supportFragmentManager
                myDialog.show(manager, "Quantity")
            }
        }
    }

}

