package com.pronline.ui.books


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.pronline.data.repositories.Book
import com.pronline.R
import com.pronline.data.network.NetPath
import kotlinx.android.synthetic.main.activity_books.*

class BooksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)
        val category:String?=intent.getStringExtra("category")
        val url= NetPath.pathApi+"get_books.php?category="+category

        val list=ArrayList<Book>()
        val rq:RequestQueue=Volley.newRequestQueue(this)
        val jar=JsonArrayRequest(Request.Method.GET, url, null, Response.Listener { response->
            for(x in 0 until response.length()-1)
            list.add(
                Book(
                    response.getJSONObject(x).getInt("id"),
                    response.getJSONObject(x).getString("title"),
                    response.getJSONObject(x).getString("author"),
                    response.getJSONObject(x).getDouble("price"),
                    response.getJSONObject(x).getString("photo")
                )
            )
            val adapter = BookAdapter(this, list)
            books_filter.layoutManager= LinearLayoutManager(this)
            books_filter.adapter=adapter

        },Response.ErrorListener { error->

            Toast.makeText(this,error.message, Toast.LENGTH_LONG).show()
        })
        rq.add(jar)


    }
}
