package com.pronline.ui.order

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pronline.R
import com.pronline.data.network.NetPath
import com.pronline.ui.home.HomeActivity
import com.pronline.util.UserInfo
import kotlinx.android.synthetic.main.activity_order.*

class OrderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
        val url = NetPath.pathApi+ "get_order.php?username="+ UserInfo.username
        val list=ArrayList<String>()
        val request: RequestQueue = Volley.newRequestQueue(this)
        val jsonArray = JsonArrayRequest(Request.Method.GET,url,null, Response.Listener { response ->
            var total:Double = 0.0
            for(x in 0 until response.length()) {
                var subtotal = response.getJSONObject(x).getDouble("price") * response.getJSONObject(x).getInt("quantity")
                total = total+subtotal
                list.add(
                    (x+1).toString()+". " + response.getJSONObject(x).getString("title") + "\n"
                            + "Written by " + response.getJSONObject(x).getString("author") + "\n"
                            + "Subtotal: " + response.getJSONObject(x).getString("price") + " x "
                            + response.getJSONObject(x).getString("quantity")+" = "+String.format("%.2f", subtotal) +" RON"
                )

            }
            list.add("Total general: "+ String.format("%.2f", total) + " RON")
            val adapterList = ArrayAdapter(this,
                android.R.layout.simple_list_item_1,list)
            order_list_view.adapter = adapterList
        }, Response.ErrorListener { error ->
            Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
        })
        request.add(jsonArray)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId==R.id.item_menu){
            val i=Intent(this,HomeActivity::class.java)
            startActivity(i)
        }
        if(item?.itemId==R.id.item_cancel){


        val url = NetPath.pathApi+ "delete_order.php?username="+UserInfo.username

        val requestQ:RequestQueue=Volley.newRequestQueue(this)
        val stringReq= StringRequest(Request.Method.GET,url,Response.Listener { response->
            val intentAct=Intent(this, HomeActivity::class.java)
            startActivity(intentAct)

        }, Response.ErrorListener { error->
            Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
        })
        requestQ.add(stringReq)

        }
        return super.onOptionsItemSelected(item)
    }

}
