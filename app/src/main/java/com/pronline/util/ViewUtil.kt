package com.pronline.util

import android.content.Context
import android.widget.Toast

class ViewUtil {
    fun Context.toast(message: String){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }
}