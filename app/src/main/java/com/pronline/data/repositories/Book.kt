package com.pronline.data.repositories

class Book {
 //   `id`, `title`, `author`, `price`, `category`, `photo`
    var id:Int
    var title:String
    var author:String
    var price:Double
    var photo:String

    constructor(id:Int, title:String, author:String, price:Double, photo:String){
        this.id=id
        this.title=title
        this.author=author
        this.price=price
        this.photo=photo
    }

}