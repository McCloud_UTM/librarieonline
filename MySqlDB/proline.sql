-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2020 at 09:36 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proline`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `bill_no` int(11) NOT NULL,
  `bill_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `author` varchar(30) NOT NULL,
  `price` double(5,2) NOT NULL,
  `category` varchar(30) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `price`, `category`, `photo`) VALUES
(1, 'Qos in retelele IP multimedia', 'Radulescu, T', 32.00, 'RETELE', 'qos-in-retelele-ip-multimedia.jpg'),
(2, 'Tehnologii fundamentale Java pentru aplicatii Web', 'Boian, F. M.', 48.00, 'RETELE', 'tehnologii-fundamentale-java-pentru-aplicatii-web.jpg'),
(3, 'Primii pasi in securitatea retelelor', 'Thomas, Tom', 38.40, 'RETELE', 'securitatea_retelelor_corint.jpg'),
(4, 'Retele locale de calculatoare-de la cablare la interconectare', 'Cebuc, E.', 35.10, 'RETELE', 'retele-locale-de-calculatoare-de-la-cablare-la-interconectare-cebuc-e-albastra.jpg'),
(5, 'CISCO - arhitecturi de securitate', 'Hundley, Kent', 99.99, 'RETELE', '24.08.05 cisco arhitectura.gif'),
(6, 'Firewalls. Protectia retelelor conectate la Internet.', 'Ogletree, T', 65.32, 'RETELE', 'big_baa106.gif'),
(7, 'Dezvoltarea aplicatiilor in C/C++ sub sistemul de operare UNIX', 'Baranga, Andrei', 23.40, 'SISTEME DE OPERARE', 'dezvoltarea-aplicatiilor-in-c-c-sub-sistemul-de-operare-unix-reeditare.jpg'),
(8, 'Start in Windows 8', 'Chip Kompakt', 20.99, 'SISTEME DE OPERARE', 'Start_in_Windows_8.jpg'),
(9, 'Introducere in sistemul de operare UNIX', 'Baranga, Andrei', 17.10, 'SISTEME DE OPERARE', 'introducere-in-sistemul-de-operare-unix.jpg'),
(10, 'UNIX Gestionarea Proceselor (Reeditare)', 'Ignat, I.', 30.60, 'SISTEME DE OPERARE', 'unix-gestionarea-proceselor-reeditare.jpg'),
(11, 'Despre Macintosh si sistemul de operare Mac OS X.', 'Hunt, Craig', 61.58, 'SISTEME DE OPERARE', 'meteor_macintosh_v_1_2.jpg'),
(12, 'Fundamentele programarii logice. Initiere in prolog', 'Bocu, Dorin', 35.10, 'LIMBAJE DE PROGRAMARE', 'fundamentele-programarii-logice-initiere-in-prolog-bocu-dorin-albastra.jpg'),
(13, 'De la C la C11. Intrebari de programare in limbajul C', 'Badulescu, L.', 32.34, 'LIMBAJE DE PROGRAMARE', 'de-la-c-la-c11-intrebari-de-programare-in-limbajul-c-lavinia-aurelian-badulescu.jpg'),
(14, 'Critica ratiunii algoritmilor de optimizare cu restricii', 'Andrei, Neculai', 152.00, 'LIMBAJE DE PROGRAMARE', 'critica-ratiunii-algoritmilor-de-optimizare-cu-restricii-neculai-andrei.jpg'),
(15, 'Echipamente de bord si navigatie aeriana. Vol. 2', 'Grigore, L.', 31.50, 'LIMBAJE DE PROGRAMARE', 'sitech-Echipamente-de-bord-si-navigatie-aeriana.-Indrumar-de-laborator-partea-a-II-a.jpg'),
(16, 'Arhitecturi sinergice de sisteme de navigatie cu componente inertiale Strap-Down', 'Grigorie, L.', 39.38, 'LIMBAJE DE PROGRAMARE', 'sitech_arhitecturi_sinergic.jpg'),
(17, 'Sisteme de baze de date. Fundamente teoretice', 'Lupsoiu, Const.', 46.20, 'BAZE DE DATE', 'sisteme-de-baze-de-date-fundamente-teoretice-constantin-lupsoiu-lupsoiu-constantin-sitech.jpg'),
(18, 'Analiza exploratorie si procesarea datelor cu simulari in Matlab', 'Gorunescu, F.', 39.60, 'BAZE DE DATE', 'analiza-exploratorie-si-procesarea-datelor-cu-simulari-in-matlab.jpg'),
(19, 'ORACLE de la 9i la 11g pentru dezvoltatorii de aplicatii - Vol. 2', 'Maftei, Eugen', 76.50, 'BAZE DE DATE', 'oracle-de-la-9i-la-11g-pentru-dezvoltatorii-de-aplicatii-volumul-2-partea-1-2.jpg'),
(20, 'Algoritmi fundamentali in utilizarea structurilor de date', '  Serban, Manuela', 26.10, 'BAZE DE DATE', 'algoritmi-fundamentali-in-utilizarea-structurilor-de-date.jpg'),
(21, 'AutoCAD 2007 pentru ingineri', 'Simion, Ionel', 20.47, 'AUTOCAD GRAFICA', 'autocad-2007-pentru-ingineri-simion-ionel-teora.jpg'),
(22, 'Programarea aplicatiilor grafice 3D cu OpenGL', 'Baciu, Rodica', 44.10, 'AUTOCAD GRAFICA', 'programarea-aplicatiilor-grafice-3d-cu-opengl.jpg'),
(23, 'QuarkXPress 3.3 pentru toti', 'Gruman, Galen', 12.67, 'AUTOCAD GRAFICA', 'big_bbd138.gif'),
(24, 'Initiere in modelarea asistata cu 3D Studio Max4', 'Ghionea, I. G.', 39.60, 'AUTOCAD GRAFICA', 'V-176WWW.jpg'),
(25, 'Macromedia Fireworks MX', 'Schulze, Patti', 43.73, 'DESIGN WEB', '02.09.05 macromedia firework mx.gif'),
(26, 'Creati propriile teme formidabile de Wordpress', 'Cole, Allan', 25.30, 'DESIGN WEB', 'Creati_propriile_teme_formidabile_de_Wordpress_3dmedia.jpg'),
(27, 'Initiere in Web Design', 'HyperText ', 32.60, 'DESIGN WEB', 'initiere-in-web-design.jpg'),
(28, 'Principii de web design atractiv', 'Beaird, Jason', 43.73, 'DESIGN WEB', 'principii-de-web-design-atractiv-beaird-jason-3d-media-communications.jpg'),
(29, 'Instrumente WEB 2.0 utilizate in educatie', 'Anghel, Traian', 32.40, 'INTERNET', 'instrumente-web-2-0-utilizate-in-educatie.jpg'),
(30, 'Introducere in XUL (XML User interface Language)', 'Acu, Calin Ioan', 21.60, 'INTERNET', 'introducere-in-xul-xml-user-interface-language.jpg'),
(31, 'Microsoft.NET Framework', ' Richter, Jeffrey', 82.46, 'INTERNET', 'microsoft-net-framework-richter-jeffrey-teora.jpg'),
(32, 'Initiere in Internet E-mail si Chat', 'Bazdoaca, N.', 14.00, 'INTERNET', '23-05-05internet.gif'),
(33, 'Microsoft Excel', 'Zsolt,Pitho', 6.40, 'OFFICE', 'microsoft-excel.jpg'),
(34, 'Microsoft Office Word 2007', 'Johnson, Steve', 47.25, 'OFFICE', 'Microsoft-Office-Word-2007-Steve-Johnson-Perspection-Inc.jpg'),
(35, 'Invata singur Microsoft Office Access 2003 in 24 de ore', 'Balter, Alison', 18.06, 'OFFICE', 'Microsoft-Office-Access-200.jpg'),
(36, 'Access 2000 - programarea bazelor de date', 'Kovacs, S.', 26.10, 'OFFICE', 'access-2000-programarea-bazelor-de-date.jpg');

-- --------------------------------------------------------


--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `username` varchar(15) NOT NULL,
  `password` varchar(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`username`, `password`, `name`, `email`) VALUES
('claudiu', '123', 'claudiu', 'claudiu@spital.ro'),
('claudiu1', '123', 'claudiu1', 'claudiu1@spital.ro'),
('claudiu2', '123', 'claudiu', 'claudiu@email.com'),
('claudiu3', '123', 'claudiu3', 'claudiu3@spital.ro'),
('claudiu4', '123', 'claudiu2', 'claudiu@yahoo.com'),
('claudiu7', '123', 'claudiu', 'claudiu@yyy.zoo');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `idorder` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `idbook` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`idorder`, `username`, `idbook`, `quantity`) VALUES
(4, 'claudiu2', 2, 3),
(21, 'claudiu', 30, 2),
(22, 'claudiu', 14, 2),
(23, 'claudiu', 14, 2),
(24, 'claudiu', 30, 2),
(25, 'claudiu', 30, 2);

-- --------------------------------------------------------


--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bill_no`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`idorder`);


--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `bill_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;



--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `idorder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
